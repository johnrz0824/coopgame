﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "SWeapon.h"
#include "Components/SkeletalMeshComponent.h"
#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"
#include "GameFramework/DamageType.h"
#include "Particles/ParticleSystem.h"
#include "Particles/ParticleSystemComponent.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "TimerManager.h"
#include "CoopGame.h"
#include "Net/UnrealNetwork.h"

static int32 DebugWeaponDrawing = 0;
FAutoConsoleVariableRef CVARDebugWeaponDrawing(TEXT("COOP.DebugWeapons"), DebugWeaponDrawing, TEXT("Draw Debug Lines for Weapons"), ECVF_Cheat);

// Sets default values
ASWeapon::ASWeapon()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	MeshComp = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("MeshComp"));
	RootComponent = MeshComp;

	MuzzleSocketName = "MuzzleFlashSocket";
	TracerTargetName = "BeamEnd";

	BaseDamage = 20.0f;
	VulnerableDamageMultiplier = 2.0f;
	FireRate = 600.0f;
	BulletSpread = 1.0f;

	NetUpdateFrequency = 66.0f;
	MinNetUpdateFrequency = 33.0f;
	SetReplicates(true);
}

void ASWeapon::BeginPlay()
{
	Super::BeginPlay();
	TimeBetweenShots = 60.0f / FireRate;
}


void ASWeapon::Fire()
{
	if (Role < ROLE_Authority)
	{
		ServerFire();
	}

	//Trace the world,from pawn eyes to crosshair location.
	AActor* owner = GetOwner();
	if (owner)
	{
		FVector eyeLocation;
		FRotator eyeRotation;
		owner->GetActorEyesViewPoint(eyeLocation, eyeRotation);

		FVector shotDirection = eyeRotation.Vector();

		float halfRad = FMath::DegreesToRadians(BulletSpread);
		shotDirection = FMath::VRandCone(shotDirection, halfRad, halfRad);

		FVector traceEnd = eyeLocation + (shotDirection * 10000);

		FCollisionQueryParams params;
		params.AddIgnoredActor(owner);
		params.AddIgnoredActor(this);
		params.bTraceComplex = true;
		params.bReturnPhysicalMaterial = true;

		FVector tracerEndPoint = traceEnd;

		EPhysicalSurface surfaceType = SurfaceType_Default;
		FHitResult hit;
		if (GetWorld()->LineTraceSingleByChannel(hit, eyeLocation, traceEnd, COLLISION_WEAPON, params))
		{
			//Blocking hits! Processing damage.
			AActor* hitActor = hit.GetActor();
			surfaceType = UPhysicalMaterial::DetermineSurfaceType(hit.PhysMaterial.Get());
			
			float actualDamage = BaseDamage;
			if (surfaceType == SURFACE_FLESHVULNERABLE)
				actualDamage *= VulnerableDamageMultiplier;
			UGameplayStatics::ApplyPointDamage(hitActor, actualDamage, shotDirection, hit, owner->GetInstigatorController(), owner, DamageType);

			PlayImpactEffects(surfaceType,hit.ImpactPoint);
			tracerEndPoint = hit.ImpactPoint;
		}

		if(DebugWeaponDrawing > 0)
			DrawDebugLine(GetWorld(), eyeLocation, traceEnd, FColor::Red, false, 1.0f, 0, 1.0f);

		PlayFireEffects(tracerEndPoint);

		if (Role == ROLE_Authority)
		{
			HitScanTrace.TraceTo = tracerEndPoint;
			HitScanTrace.SurfaceType = surfaceType;
		}

		LastFireTime = GetWorld()->TimeSeconds;
	}
}

//Rpc需要這樣實作
void ASWeapon::ServerFire_Implementation()
{
	Fire();
}
//實作WithValidation
bool ASWeapon::ServerFire_Validate()
{
	return true;
}

void ASWeapon::OnRep_HitScanTrace()
{
	PlayFireEffects(HitScanTrace.TraceTo);
	PlayImpactEffects(HitScanTrace.SurfaceType, HitScanTrace.TraceTo);
}

void ASWeapon::StartFire()
{
	float firstDelay = FMath::Max(LastFireTime + TimeBetweenShots - GetWorld()->TimeSeconds, 0.0f);

	GetWorldTimerManager().SetTimer(TimerHandle_TimeBetweenShots,this, &ASWeapon::Fire, TimeBetweenShots, true, firstDelay);
}

void ASWeapon::StopFire()
{
	GetWorldTimerManager().ClearTimer(TimerHandle_TimeBetweenShots);
}

void ASWeapon::PlayFireEffects(FVector traceEnd)
{
	if (MuzzleEffect)
	{
		UGameplayStatics::SpawnEmitterAttached(MuzzleEffect, MeshComp, MuzzleSocketName);
	}

	if (TracerEffect)
	{
		FVector MuzzleLocation = MeshComp->GetSocketLocation(MuzzleSocketName);

		UParticleSystemComponent* tracerComp = UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), TracerEffect, MuzzleLocation);
		if (tracerComp)
		{
			tracerComp->SetVectorParameter(TracerTargetName, traceEnd);
		}
	}

	APawn* owner = Cast<APawn>(GetOwner());
	if (owner)
	{
		APlayerController* playerController = Cast<APlayerController>(owner->GetController());
		if (playerController)
		{
			playerController->ClientPlayCameraShake(CameraShakeClass);
		}
	}
}

void ASWeapon::PlayImpactEffects(EPhysicalSurface surfaceType, FVector impactPoint)
{
	UParticleSystem* selectedEffect = nullptr;
	switch (surfaceType)
	{
	case SURFACE_FLESHDEFAULT:
	case SURFACE_FLESHVULNERABLE:
		selectedEffect = FleshImpactEffect;
		break;
	default:
		selectedEffect = DefaultImpactEffect;
		break;
	}
	FVector muzzleLocation = MeshComp->GetSocketLocation(MuzzleSocketName);

	FVector shotDir = impactPoint - muzzleLocation;
	shotDir.Normalize();

	if (selectedEffect)
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), selectedEffect, impactPoint, shotDir.Rotation());
}

void ASWeapon::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	//複製給除了自己的所有Client
	DOREPLIFETIME_CONDITION(ASWeapon, HitScanTrace,COND_SkipOwner);
}