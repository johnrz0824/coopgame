// Fill out your copyright notice in the Description page of Project Settings.

#include "SHealthComponent.h"
#include "GameFramework/Actor.h"
#include "Net/UnrealNetwork.h"
#include "Engine/World.h"
#include "SGameMode.h"

// Sets default values for this component's properties
USHealthComponent::USHealthComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	DefaultHealth = 100.0f;
	TeamNumber = 255;
	IsDead = false;

	SetIsReplicated(true);
}


float USHealthComponent::GetHealth() const
{
	return Health;
}

// Called when the game starts
void USHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	//Only hook if we are server
	if (GetOwnerRole() == ROLE_Authority)
	{
		AActor* owner = GetOwner();
		if (owner)
		{
			owner->OnTakeAnyDamage.AddDynamic(this, &USHealthComponent::HandleTakeAnyDamage);
		}
	}
	Health = DefaultHealth;
}

void USHealthComponent::OnRep_Health(float oldHealth)
{
	float damage = Health - oldHealth;
	OnHealthChanged.Broadcast(this, Health, damage, nullptr, nullptr, nullptr);
}

void USHealthComponent::HandleTakeAnyDamage(AActor * DamagedActor, float Damage, const UDamageType * DamageType, AController * InstigatedBy, AActor * DamageCauser)
{

	if (Damage <= 0 || IsDead) { return; }
	if ( DamageCauser != DamagedActor && IsFriendly(DamagedActor, DamageCauser)) return;
	Health = FMath::Clamp(Health - Damage, 0.0f, DefaultHealth);
	IsDead = Health <= 0;
	OnHealthChanged.Broadcast(this, Health, Damage, DamageType, InstigatedBy, DamageCauser);

	if (IsDead)
	{
		IsDead = true;
		ASGameMode* GM = Cast<ASGameMode>(GetWorld()->GetAuthGameMode());
		if (GM)
		{
			GM->OnActorKilled.Broadcast(DamagedActor, DamageCauser, InstigatedBy);
		}
	}
}

void USHealthComponent::Heal(float healAmount)
{
	if (healAmount <= 0 || Health <=0) return;
	Health = FMath::Clamp(Health + healAmount, 0.0f, DefaultHealth);
	OnHealthChanged.Broadcast(this, Health, -healAmount, nullptr, nullptr, nullptr);
}

bool USHealthComponent::IsFriendly(AActor * actorA, AActor * actorB)
{
	if (actorA == nullptr || actorB == nullptr)return false;

	USHealthComponent* healthCompA = Cast<USHealthComponent>(actorA->GetComponentByClass(USHealthComponent::StaticClass()));
	USHealthComponent* healthCompB = Cast<USHealthComponent>(actorB->GetComponentByClass(USHealthComponent::StaticClass()));

	if (healthCompA == nullptr || healthCompB == nullptr)return false;

	return healthCompA->TeamNumber == healthCompB->TeamNumber;
}

void USHealthComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(USHealthComponent, Health);
}
