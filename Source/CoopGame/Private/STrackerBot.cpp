// Fill out your copyright notice in the Description page of Project Settings.

#include "STrackerBot.h"
#include "CoopGame.h"
#include "Components/StaticMeshComponent.h"
#include "Kismet/GameplayStatics.h"
#include "NavigationSystem.h"
#include "NavigationPath.h"
#include "GameFramework/Character.h"
#include "SHealthComponent.h"
#include "Materials/MaterialInstanceDynamic.h"
#include "Components/SphereComponent.h"
#include "SCharacter.h"
#include "TimerManager.h"
#include "Sound/SoundCue.h"
#include "DrawDebugHelpers.h"

static int32 DebugTrackerBotDrawing = 0;
FAutoConsoleVariableRef CVARDebugTrackerBotDrawing(
	TEXT("Coop.DebugTrackerBot"),
	DebugTrackerBotDrawing,
	TEXT("Draw Debug Lines for TrackerBot"),
	ECVF_Cheat);

// Sets default values
ASTrackerBot::ASTrackerBot()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	MeshComp->SetCanEverAffectNavigation(false);
	MeshComp->SetSimulatePhysics(true);
	RootComponent = MeshComp;

	HealthComp = CreateDefaultSubobject<USHealthComponent>(TEXT("Health"));
	HealthComp->OnHealthChanged.AddDynamic(this, &ASTrackerBot::OnHealthChanged);

	SphereComp = CreateDefaultSubobject<USphereComponent>(TEXT("Sphere"));
	SphereComp->SetSphereRadius(200);
	SphereComp->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	SphereComp->SetCollisionResponseToAllChannels(ECR_Ignore);
	SphereComp->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);
	SphereComp->SetCollisionResponseToChannel(COLLISION_TRACKERENEMY, ECR_Ignore);
	SphereComp->SetupAttachment(RootComponent);

	MovementForce = 1000.0f;
	ExplosionRadius = 350;
	ExplosionDamage = 60;
	SelfDamageInterval = 0.5f;
	bUseVelocityChange = false;
	RequiredDistanceToTarget = 100.0f;
	MaxPowerLevel = 4;
	bExploded = false;
	bStartedSelfDestruction = false;
}

// Called when the game starts or when spawned
void ASTrackerBot::BeginPlay()
{
	Super::BeginPlay();
	if (Role != ROLE_Authority) return;
	//Find initial move-to
	NextPathPoint = GetNextPathPoint();

	FTimerHandle timerHandle_CheckPowerLevel;
	GetWorldTimerManager().SetTimer(timerHandle_CheckPowerLevel, this, &ASTrackerBot::OnCheckNearbyBots, 1, true, 0.0f);

}

FVector ASTrackerBot::GetNextPathPoint()
{
	AActor* bestTarget = nullptr;
	float nearestTargetDistance = MAX_FLT;

	for (FConstPawnIterator it = GetWorld()->GetPawnIterator(); it; ++it)
	{
		APawn* testPawn = it->Get();
		if (testPawn == nullptr || USHealthComponent::IsFriendly(testPawn,this)) continue;
		USHealthComponent* healthComp = Cast<USHealthComponent>(testPawn->GetComponentByClass(USHealthComponent::StaticClass()));
		if (healthComp && healthComp->GetHealth() > 0)
		{
			float distance = (testPawn->GetActorLocation() - GetActorLocation()).Size();

			if (distance < nearestTargetDistance)
			{
				nearestTargetDistance = distance;
				bestTarget = testPawn;
			}
		}
	}
	if (bestTarget)
	{
		UNavigationPath* path = UNavigationSystemV1::FindPathToActorSynchronously(this, GetActorLocation(), bestTarget);
		GetWorldTimerManager().ClearTimer(TimerHandle_RefreshPath);
		GetWorldTimerManager().SetTimer(TimerHandle_RefreshPath, this, &ASTrackerBot::RefreshPath, 5.0f, false,0.0f);
		if (path && path->PathPoints.Num() > 1)
		{
			return path->PathPoints[1];
		}
	}
	return GetActorLocation();
}

void ASTrackerBot::SelfDestruct()
{
	if (bExploded) return;
	bExploded = true;
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ExplosionEffect, GetActorLocation());
	MeshComp->SetVisibility(false, true);
	MeshComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	if (Role == ROLE_Authority)
	{
		TArray<AActor*> ignores;
		ignores.Add(this);
		float actualDamage = ExplosionDamage + (ExplosionDamage * PowerLevel);
		UGameplayStatics::ApplyRadialDamage(this, actualDamage, GetActorLocation(), ExplosionRadius, nullptr, ignores, this, GetInstigatorController(), true);
		UGameplayStatics::SpawnSoundAttached(ExplosionSound, RootComponent, NAME_None, FVector(0, 0, 0), EAttachLocation::KeepRelativeOffset);
		//Destroy();
		SetLifeSpan(2.0f);
	}
}

void ASTrackerBot::NotifyActorBeginOverlap(AActor * OtherActor)
{
	Super::NotifyActorBeginOverlap(OtherActor);
	if (bStartedSelfDestruction || bExploded) return;
	ASCharacter* playerPawn = Cast<ASCharacter>(OtherActor);
	if (playerPawn && !USHealthComponent::IsFriendly(this,OtherActor))
	{
		if (Role == ROLE_Authority)
		{
			GetWorldTimerManager().SetTimer(TimerHandle_SelfDamage, this, &ASTrackerBot::DamageSelf, SelfDamageInterval, true, 0.0f);
		}
		bStartedSelfDestruction = true;
		UGameplayStatics::SpawnSoundAttached(SelfDestructionSound, RootComponent, NAME_None, FVector(0, 0, 0), EAttachLocation::KeepRelativeOffset);
	}
}

void ASTrackerBot::DamageSelf()
{
	UGameplayStatics::ApplyDamage(this, 20, GetInstigatorController(), this, nullptr);
}

void ASTrackerBot::OnCheckNearbyBots()
{
	const float radius = 600;
	FCollisionShape collShape = FCollisionShape::MakeSphere(radius);
	FCollisionObjectQueryParams params;
	params.AddObjectTypesToQuery(ECC_PhysicsBody);
	params.AddObjectTypesToQuery(ECC_Pawn);

	TArray<FOverlapResult> overlaps;
	GetWorld()->OverlapMultiByObjectType(overlaps,GetActorLocation(), FQuat::Identity, params, collShape);
	if(DebugTrackerBotDrawing)
		DrawDebugSphere(GetWorld(), GetActorLocation(), radius, 24, FColor::Red, false);

	int botCount = 0;
	for (FOverlapResult result : overlaps)
	{
		ASTrackerBot* bot = Cast<ASTrackerBot>(result.GetActor());
		if (bot && bot != this)
		{
			botCount++;
		}
	}

	PowerLevel = FMath::Clamp(botCount, 0, MaxPowerLevel);

	if (MatInst == nullptr)
	{
		MatInst = MeshComp->CreateAndSetMaterialInstanceDynamicFromMaterial(0, MeshComp->GetMaterial(0));
	}
	
	if (MatInst)
	{
		float alpha = PowerLevel / (float)MaxPowerLevel;
		MatInst->SetScalarParameterValue("PowerLevelAlpha", alpha);
	}
}

void ASTrackerBot::RefreshPath()
{
	NextPathPoint = GetNextPathPoint();
}

// Called every frame
void ASTrackerBot::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (Role != ROLE_Authority) return;
	if (bExploded) return;
	float distanceToTarget = (GetActorLocation() - NextPathPoint).Size();
	if (distanceToTarget <= RequiredDistanceToTarget)
	{
		NextPathPoint = GetNextPathPoint();
	}
	else
	{
		FVector forceDirection = GetNextPathPoint() - GetActorLocation();
		forceDirection.Normalize();
		forceDirection *= MovementForce;
		MeshComp->AddForce(forceDirection, NAME_None, bUseVelocityChange);
	}
}

void ASTrackerBot::OnHealthChanged(USHealthComponent * healthComponent, float Health, float HealthDelta, const UDamageType * DamageType, AController * InstigatedBy, AActor * DamageCauser)
{
	if (MatInst == nullptr)
		MatInst = MeshComp->CreateAndSetMaterialInstanceDynamicFromMaterial(0, MeshComp->GetMaterial(0));
	if (MatInst)
		MatInst->SetScalarParameterValue("LastTimeDamageTaken", GetWorld()->TimeSeconds);

	if (Health <= 0.0f && !bExploded)
	{
		SelfDestruct();

		/*GetMovementComponent()->StopMovementImmediately();
		GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);

		DetachFromControllerPendingDestroy();
		SetLifeSpan(10.0f);*/
	}
}

