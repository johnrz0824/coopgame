// Fill out your copyright notice in the Description page of Project Settings.


#include "SGameState.h"
#include "Net/UnrealNetwork.h"

void ASGameState::OnRep_WaveState(EWaveState oldState)
{
	WaveStateChanged(WaveState,oldState);
}

void ASGameState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(ASGameState, WaveState);
}

void ASGameState::SetWaveState(EWaveState newState)
{
	if (Role == ROLE_Authority)
	{
		EWaveState oldState = WaveState;
		WaveState = newState;
		OnRep_WaveState(oldState);
	}
}