// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SGameMode.generated.h"

/**
 * 
 */
enum class EWaveState : uint8;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnActorKilled, AActor*, victimActor, AActor*, killerActor, AController*,killerController);

UCLASS()
class COOPGAME_API ASGameMode : public AGameModeBase
{
	GENERATED_BODY()

protected:
	FTimerHandle TimerHandle_BotSpawner;
	FTimerHandle TimerHandle_NextWaveStart;

	int32 WaveCount;
	int32 NumberOfBotsToSpawn;

	UPROPERTY(EditDefaultsOnly,Category="GameMode")
	float TimeBetweenWaves;
	//Hook for NP to spawn a single bot
	UFUNCTION(BlueprintImplementableEvent,Category="GameMode")
		void SpawnNewBot();

	void SpawnBotTimerElasped();
	void StartWave();
	void EndWave();
	void PrepareForNextWave();
	void CheckWaveState();
	void CheckAnyPlayerAlive();
	void GameOver();
	void SetWaveState(EWaveState newState);
	void RestartDeadPlayers();
public:
	ASGameMode();

	virtual void StartPlay()override;

	virtual void Tick(float DeltaSeconds) override;

	UPROPERTY(BlueprintAssignable, Category="GameMode")
	FOnActorKilled OnActorKilled;
};
